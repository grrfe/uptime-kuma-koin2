package fe.uptimekuma.koin.module.service

import fe.koin.helper.util.singleModule
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

val defaultUptimeKumaScheduledExecutorModule = singleModule<ScheduledExecutorService> {
    Executors.newScheduledThreadPool(1)
}
