package fe.uptimekuma.koin.module.uptime

import fe.httpkt.Request
import fe.koin.helper.util.singleModule
import fe.uptimekuma.koin.config.UptimeKumaConfig

val uptimeKumaModule = singleModule {
    UptimeKuma(get())
}

class UptimeKuma(
    private val config: UptimeKumaConfig,
    private val request: Request = Request(),
) {
    val ping = Runnable {
        request.get(config.pingUrl, forceSend = true)
    }

    val intervalSeconds = config.intervalSeconds
    val enabled = config.enabled
}
