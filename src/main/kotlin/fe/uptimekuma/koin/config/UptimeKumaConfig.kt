package fe.uptimekuma.koin.config

import fe.koin.helper.config.KoinConfig

data class UptimeKumaConfig(
    val enabled: Boolean,
    val pingUrl: String,
    val intervalSeconds: Long = 10,
) : KoinConfig<UptimeKumaConfig>
