package fe.uptimekuma.koin.init

import fe.koin.helper.init.KoinInit2
import fe.uptimekuma.koin.module.uptime.UptimeKuma
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

object UptimeKumaInit : KoinInit2<UptimeKuma, ScheduledExecutorService> {

    override fun init(t: UptimeKuma, t2: ScheduledExecutorService) {
        if (t.enabled) {
            t2.scheduleAtFixedRate(t.ping, 0, t.intervalSeconds, TimeUnit.SECONDS)
        }
    }
}
