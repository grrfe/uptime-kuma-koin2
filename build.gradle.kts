plugins {
    kotlin("jvm")
    `maven-publish`
    id("net.nemerosa.versioning")
}

group = findProperty("_group") ?: "fe.uptimekuma.koin"
version = findProperty("_version") ?: versioning.info.tag ?: versioning.info.full ?: "0.0.0"

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    api(Koin.core)
    api(libs.koin.helper)
    api(libs.com.gitlab.grrfe.httpkt.core)
}


publishing {
    publications {
        create<MavenPublication>("maven") {
            findProperty("_artificat")?.let { artifactId = it.toString() }
            from(components["java"])
        }
    }
}
